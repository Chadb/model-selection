'''
Written by Chad Brisbois
This script will calculate the signficance of a model using the TS from a log-likelihood ratio and the number of degrees of freedom in the model.

This relies on Wilks' Theorem, that the TS is distributed like Chi2 for nested models

Wilks, S. S. (1938). The Large-Sample Distribution of the Likelihood Ratio for Testing Composite Hypotheses. https://doi.org/10.1214/aoms/1177732360
'''
import scipy.stats as st
from scipy.special import erfinv
import numpy as np
import argparse as ap

# This is the Quantile function for the normal distribution
def p2z(p,one_side=True):

    # Make it one-sided per typical
    if one_side:
        q=p/2.0

    z = np.sqrt(2) * erfinv(2*q-1)
    return -z

parse = ap.ArgumentParser()
parse.add_argument("TS" , default=25.0, type=float, help="The TS between two models")
parse.add_argument("dof", default=1   , type=int, help="The degrees of freedom difference between the two models")
#parse.add_argument("--table", action='store_true', required=False, help="Make table ")
args = parse.parse_args()



ts = args.TS
df = args.dof

#if args.table:
#    sigma = [1,2,3,4,5]
#    ts = [ s*s for s in sigma ]
#    dof = range(20)[1:]
    
    
    
# Calculate the p value with the survival function of the Chi2 distribution
p = st.chi2.sf(ts,df)


z = p2z(p)


#print(p)
#print out the sigmas
print("{0:0.3g}".format(z))
