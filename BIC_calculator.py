'''
Written by Chad Brisbois
This script will calculate the preferred model between two models using the Bayesian Information Criterion. This method does _not_ require the models to be nested. 


This relies on Table from Section 3.2 of 
Kass, R. E., & Raftery, A. E. (1995). Bayes Factors.https://doi.org/10.1080/01621459.1995.10476572
'''
from collections import OrderedDict
import argparse as ap
import numpy as np

ranges=OrderedDict()
ranges["Not worth more than a bare mention"] = [ 0., 2.]
ranges["Positive"]                           = [ 2., 6.]
ranges["Strong"]                             = [ 6., 10.]
ranges["Very Strong"]                        = [10., np.inf] 


parse = ap.ArgumentParser()
parse.add_argument("--model_BIC" ,dest='bic', default=[100.0, 100.0], nargs = 2, type=float, 
                   help="The log(likelihood) of models A and B respectively")
parse.add_argument("--table", action='store_true', required=False, help="Display table")
args = parse.parse_args()



def display(info):
    # Display table and exit
    print("  DeltaBIC           Evidence against Model A")
    print("  --------           ------------------------")
    for key in info.keys():
        rang=info[key]
        first = "{0} - {1}".format(*rang)
        print(" {0:^10}  {1:^40}".format(first,key))

if args.table:
    display(ranges)
    exit(0)

bicA = args.bic[0]
bicB = args.bic[1]

deltaBIC = bicB - bicA

if deltaBIC < 0:
    deltaBIC *= -1
    worse="BIC_A"
else:
    worse="BIC_A"




for key in ranges.keys():
    rang=ranges[key]
    
    if (rang[0]<deltaBIC<=rang[1]):
        evidence=key


print("DeltaBIC              : {0:<4g}".format(deltaBIC))
print("Evidence against {0}: {1}".format(worse,evidence))
